from django.db import models


class Corporation(models.Model):
    name = models.CharField(max_length=200)
    info = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Group(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Cook(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Adress(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Dish(models.Model):
    article = models.IntegerField(default=0)
    name = models.CharField(max_length=200)
    weight = models.IntegerField(default=0)
    consist = models.CharField(max_length=200)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    Corporation = models.ForeignKey(Corporation, on_delete=models.CASCADE)
    Adress = models.ForeignKey(Adress, on_delete=models.CASCADE)
    Cook = models.ForeignKey(Cook, on_delete=models.CASCADE)

    def __str__(self):
        return self.name + str(self.article) + str(self.weight) + self.consist + str(self.group) + str(self.Corporation) + str(self.Adress) + str(self.Cook)



