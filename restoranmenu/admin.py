from django.contrib import admin
from .models import *
admin.site.register(Corporation)
admin.site.register(Cook)
admin.site.register(Adress)
admin.site.register(Group)
@admin.register(Dish)
class DishAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = ('name', 'weight', 'consist',
                'group', 'Adress', 'Corporation', 'Cook')

